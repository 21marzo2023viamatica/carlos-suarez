package com.asignacion.constant;

public final class asignacionConstant {
    
    //ESTADO
    public static final Integer ESTADO_ACTIVO = 1;
    public static final Integer ESTADO_INACTIVO = 0;
    
    //API VERSION
    public static final String API_VERSION = "/v1";
    
    //PATH DEL SISTEMA ASIGNACION
    public static final String RESOURCE_GENERIC = API_VERSION + "/asignacion";
    public static final String RESOURCE_EMPLEADOS = "/empleados";
    public static final String RESOURCE_EMPLEADOS_EMPLEADO = "/empleado";
    public static final String RESOURCE_GENERIC_ID = "/{id}";
    
    public static final String RESOURCE_CREDENCIALES = "/{correo}/{contrasenia}";
    
    public static final String CLIENT_FRONTEND = "*";
    
}
