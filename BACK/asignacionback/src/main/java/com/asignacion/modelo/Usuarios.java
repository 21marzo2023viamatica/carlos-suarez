package com.asignacion.modelo;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.sql.Date;

import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@Entity
@Table(name = "usuarios")
public class Usuarios {

    @Id
    @Column(name = "idusuarios")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idusuarios;

    @NotNull
    @Column(name = "nombres", length = 100)
    private String nombres;

    @NotNull
    @Column(name = "apellidos", length = 100)
    private String apellidos;

    @NotNull
    @Column(name = "fnacimiento")
    private Date fnacimiento;

    @NotNull
    @Column(name = "correo", length = 100)
    private String correo;

    @NotNull
    @Column(name = "contrasenia", length = 100)
    private String contrasenia;

    @NotNull
    @Column(name = "area", length = 100)
    private String area;

    @NotNull
    @Column(name = "rol", length = 100)
    private String rol;

    @NotNull
    @Column(name = "identificador", length = 10)
    private String identificador;

    @NotNull
    @Column(name = "estado", columnDefinition = "int default '1'")
    private String estado;

    public int getIdusuarios() {
        return idusuarios;
    }

    public void setIdusuarios(int idusuarios) {
        this.idusuarios = idusuarios;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public Date getFnacimiento() {
        return fnacimiento;
    }

    public void setFnacimiento(Date fnacimiento) {
        this.fnacimiento = fnacimiento;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Usuarios() {
        super();
    }

    @Override
    public String toString() {
        return "Usuarios [correo=" + correo + ", contrasenia=" + contrasenia + " ]";
    }

}
