package com.asignacion.serviceImpl;

import com.asignacion.dbutil.DBUtil;
import com.asignacion.modelo.Usuarios;
import com.asignacion.service.UsuariosService;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.asignacion.repository.UsuariosRepository;

@Service
public class UsuariosServiceImpl implements UsuariosService{
    
    @Autowired
    UsuariosRepository usuariosRepository;
/*    
    public UsuariosServiceImpl(UsuariosRepository usuariosRepository) {
        this.usuariosRepository = usuariosRepository;
    }
 */   
    // *************************************************************************
    // CREAR, BUSCAR, ACTUALIZAR USUARIOS DE BASE DE DATOS
    // *************************************************************************
    
    @Override
    public boolean create(Usuarios usuarios){
        usuariosRepository.save(usuarios);
        return true;
    }
    
    @Override
    public Usuarios update(Usuarios usuarios, Integer id) {
        Usuarios bean = this.usuariosRepository.findById(id).get();
        bean.setNombres(usuarios.getNombres());
        return this.usuariosRepository.save(bean);
    }
    
    @Override
    public Usuarios findById(Integer id) {
        Usuarios bean = this.usuariosRepository.findById(id).get();
        return bean;
    }
    
    @Override
    public List<Usuarios> findAll() {
        return this.usuariosRepository.findAll();
    }
    
    // *************************************************************************
    // CONECTAR A BASE DE DATOS
    // *************************************************************************
    
    Connection connection;
    int bandera;
    
    public UsuariosServiceImpl() throws SQLException {
        connection = DBUtil.getConnection();
    }
    
    @Override
    public int validacionLogin(String correo, String contrasenia) {
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM usuarios WHERE correo='"+correo+"'");
            ResultSet rs = statement.executeQuery();
            while(rs.next()){
                if(rs.getString(5).equals(correo) && rs.getString(6).equals(contrasenia)){
                    bandera = 1;
                    System.out.println("Acceso CORRECTO..");
                } else {
                    bandera = 0;
                    System.out.println("Acceso INCORRECTO.. Verificar el correo o la contraseña..");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return bandera;
    }

}
