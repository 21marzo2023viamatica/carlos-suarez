package com.asignacion.service;

import com.asignacion.modelo.Usuarios;
import java.util.List;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuariosService {
    
    public int validacionLogin(String correo, String contrasenia);
    
    public Usuarios update(Usuarios usuarios, Integer id);
    public Usuarios findById(Integer id);
    public List<Usuarios> findAll();
    
    public boolean create(Usuarios usuarios);
    
}
