package com.asignacion.controller;

import com.asignacion.constant.asignacionConstant;
import com.asignacion.modelo.Usuarios;
import com.asignacion.service.UsuariosService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(asignacionConstant.RESOURCE_GENERIC)
@CrossOrigin(asignacionConstant.CLIENT_FRONTEND)
public class UsuariosController {

    @Autowired
    private UsuariosService usuariosService;

    @GetMapping(asignacionConstant.RESOURCE_EMPLEADOS + asignacionConstant.RESOURCE_CREDENCIALES)
    public int UsuarioLogin(@PathVariable("correo") String nickname1, @PathVariable("contrasenia") String contrasenia1) {
        int bandera = usuariosService.validacionLogin(nickname1, contrasenia1);
        if (bandera == 0) {
            return 0;
        }
        return bandera;
    }

    public UsuariosController(UsuariosService usuariosService) {
        this.usuariosService = usuariosService;
    }

    @GetMapping(asignacionConstant.RESOURCE_EMPLEADOS + asignacionConstant.RESOURCE_EMPLEADOS_EMPLEADO)
    public List<Usuarios> findAll() {
        return this.usuariosService.findAll();
    }

    @GetMapping(asignacionConstant.RESOURCE_EMPLEADOS + asignacionConstant.RESOURCE_EMPLEADOS_EMPLEADO + asignacionConstant.RESOURCE_GENERIC_ID)
    public Usuarios findById(@PathVariable Integer id) {
        return this.usuariosService.findById(id);
    }

    @PutMapping(asignacionConstant.RESOURCE_EMPLEADOS + asignacionConstant.RESOURCE_EMPLEADOS_EMPLEADO + asignacionConstant.RESOURCE_GENERIC_ID)
    public Usuarios update(@RequestBody Usuarios usuarios, @PathVariable Integer id) {
        return this.usuariosService.update(usuarios, id);
    }

    @PostMapping(asignacionConstant.RESOURCE_EMPLEADOS + asignacionConstant.RESOURCE_EMPLEADOS_EMPLEADO + "/save")
    public ResponseEntity<Object> crearUsuario(@RequestBody Usuarios usuarios) {
        usuariosService.create(usuarios);
        return ResponseEntity.ok("Ok");
    }
}
